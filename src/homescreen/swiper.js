import React, { Component } from 'react';
import SwipeableParallaxCarousel from 'react-native-swipeable-parallax-carousel';


export default class Swiper extends Component {

    constructor() {
        super();
        this.state = {
            datacarousel: [],
            datacarousel1: [{ imagePath: null }]
        };
    }

    componentDidMount() {
        fetch("http://ec2-54-255-226-10.ap-southeast-1.compute.amazonaws.com:9009/api/banner", {
            method: "GET",
            headers: {
                'Authorization': 'Basic dGVsa29tOmRhMWMyNWQ4LTM3YzgtNDFiMS1hZmUyLTQyZGQ0ODI1YmZlYQ=='
            }
        })
            .then((response) => response.json())
            .then((data) => {

                this.setState({
                    datacarousel: data.data
                });

            })
            .catch((error) => {
                console.log(error);
            })
    }

    render() {
        let carouseldata = [];

        this.state.datacarousel.map((item, idx) => {
            let data = {
                id: item._id,
                imagePath: item.imagesBanner
            }
            carouseldata.push(data)
        })
        this.state.datacarousel = carouseldata;
        return (
            <SwipeableParallaxCarousel
                data={carouseldata}
                navigation={true}
                navigationType={'dots'}
                height={150}
                delay={2000}
            />
        );
    }
}