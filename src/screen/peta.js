import React, { Component } from 'react';
import { Platform, Text, View, StyleSheet, AsyncStorage } from 'react-native';
import MapView from 'react-native-maps';
import { Container, Content } from 'native-base';
import { AppHeader, AppFooter } from "../app-nav/index";

const GEOLOCATION_OPTIONS = { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 };
var Analytics = require('react-native-firebase-analytics');

export default class Peta extends Component {
    constructor(props) {
        super(props);
        this.state = {
            jsonData: '',
            marker: this.props.navigation.state.params.location
        }
    }

    componentWillMount() {
        Analytics.setUserId('6');
        Analytics.setUserProperty('peta', 'propertyValue');

        Analytics.logEvent('peta_poi', {
            'item_id': this.state.marker.name
        });
        Analytics.setScreenName('peta')
    }

    componentDidMount() {
        this.setState({
            region: {
                latitude: this.state.marker.geometry.location.lat,
                longitude: this.state.marker.geometry.location.lng,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0021,
            }
        })
    }

    render() {
        return (
            <Container>
                <AppHeader navigation={this.props.navigation} title="Point of Interest" />
                <Content>
                </Content>
                <View style={styles.container}>
                    <MapView
                        style={styles.map}
                        showsUserLocation={true}
                        region={this.state.region}
                        onRegionChange={this.onRegionChange}
                    >
                        <MapView.Marker
                            coordinate={{
                                latitude: this.state.marker.geometry.location.lat,
                                longitude: this.state.marker.geometry.location.lng
                            }}
                            title={this.state.marker.name}
                            description={this.state.marker.location}
                        />
                    </MapView>
                </View>
            </Container>
        );
    }
}


const styles = StyleSheet.create({
    baseText: {
        fontFamily: 'Ubuntu-Light',

    },
    titleText: {
        fontFamily: 'Ubuntu-Regular',
        fontSize: 30,
        fontWeight: 'bold',
    },
    contentContainer: {
        borderWidth: 0,
        borderColor: '#FFF',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    footer: {
        backgroundColor: "#fff"
    },
    map: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 40

    },
    container: {
        backgroundColor: "#FFF",
        position: 'absolute',
        top: 80,
        left: 0,
        right: 0,
        bottom: 0,
        justifyContent: 'flex-end',
        alignItems: 'center'
    }
});